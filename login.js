// Note: For all file operations use promises with fs

// Q1. Create 2 files simultaneously (without chaining two function 
// calls one after the other).
// Wait for 2 seconds and starts deleting them one after another.
// (Finish deleting all the files no matter what happens 
// with the creation code. Ensure that this is tested well)

const fs = require("fs")
const path = require("path")

let file1 = "raju.json"
let file2 = "aashu.json"

let promise = [
  fs.promises.writeFile(path.join(__dirname, file2), JSON.stringify(["Aashu", 29])),
  fs.promises.writeFile(path.join(__dirname, file1), JSON.stringify(["Raju", 26])),

  new Promise(() => {

    setTimeout(() => {
      fs.promises.unlink(path.join(__dirname, file1))
        .catch((err) => {
          console.log(err)
        })
      fs.promises.unlink(path.join(__dirname, file2))
        .catch((err) => {
          console.log(err)
        })
    }, 2 * 1000)
  })
]

Promise.all([promise])
  .then(() => {
    console.log("All Files Created deleted")
  })
  .catch((err) => {
    console.log(err)
  })





// Q2.Create a new file with lipsum data(you can google and get this). 
// Do File Read and write data to another file
// Delete the original file

// A.using promises chaining

let lipsum_data = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ornare turpis sit amet ex feugiat tincidunt. Duis vel odio nisl. Nam in venenatis odio. Morbi commodo dui nec neque euismod, sit amet rhoncus elit accumsan. Sed ut venenatis erat. Ut at condimentum metus, vel iaculis leo. Nam mattis efficitur felis nec fermentum. Nullam efficitur augue aliquet, ullamcorper ante quis, semper elit. Quisque rutrum mauris quis risus commodo, lobortis aliquet nunc consectetur. Duis tempus quam sed elit finibus ultricies. Ut mi odio, fermentum nec ipsum et, iaculis fringilla mauris. Quisque ut tortor non justo hendrerit dictum. Nullam ornare porta purus ac pulvinar. Duis auctor bibendum efficitur. Sed iaculis ligula a felis ultrices, vitae fermentum risus aliquam. Proin cursus suscipit nunc, a sagittis ex rutrum quis.

Suspendisse ultricies, lacus eget convallis fermentum, urna arcu lacinia libero, eu sodales ipsum felis ut lectus. Suspendisse egestas tellus a nisi hendrerit pretium. Sed commodo eget felis a dapibus. Mauris ac erat a lacus ultricies porta a in risus. Integer a pulvinar mauris, ac congue eros. Donec commodo urna lorem, ac scelerisque enim condimentum ac. Aliquam quis arcu libero. Fusce et eleifend nulla. Curabitur a ligula dapibus, luctus augue eget, vestibulum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus lobortis, sem a finibus congue, ex turpis cursus odio, vitae vulputate nisl odio sed odio. Mauris rhoncus purus quis magna tristique, sed hendrerit felis vestibulum.

Nam sit amet metus at massa vehicula euismod non nec libero. Aliquam erat volutpat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris feugiat dignissim feugiat. Pellentesque varius facilisis purus, nec posuere enim pulvinar sed. Aliquam eleifend, lectus vel accumsan rutrum, dolor est aliquet dui, in dictum velit augue at nibh. Curabitur porttitor venenatis vestibulum.

Nunc elementum, dui eu feugiat feugiat, nulla tortor consequat libero, a posuere ligula mauris ut lectus. Fusce at sapien lacus. Aenean faucibus neque ac ornare rhoncus. Vestibulum dictum dolor ac nisi sollicitudin viverra eget at nisi. Duis non pellentesque orci, ut euismod lorem. Aliquam mattis urna et dapibus dictum. Fusce ut lacus a orci consequat pulvinar nec eu risus. Quisque eget luctus tellus, eget tristique lectus. Integer ornare dapibus interdum. Maecenas condimentum turpis ut tempor gravida. Aenean tempor iaculis sodales. Sed quis sapien ut lectus placerat congue tincidunt sit amet enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu sodales nibh.

Donec consequat lectus leo, eu vestibulum quam faucibus quis. Proin porta ex tempus arcu pulvinar, vel faucibus lacus facilisis. Nunc eleifend, metus at ultricies consectetur, eros risus vehicula magna, viverra placerat magna elit sit amet risus. Curabitur semper lectus in massa ultricies, tincidunt sodales mi vestibulum. Nulla leo sapien, auctor sed orci vel, finibus maximus ante. Donec faucibus porta venenatis. Ut condimentum tempus nisi. Nunc ornare nisl a lorem pharetra, sit amet faucibus ipsum commodo. Ut neque nisi, interdum non lorem sit amet, laoreet rhoncus justo.`



fs.promises.writeFile(path.join(__dirname, "lipsum.txt"), lipsum_data)
  .then(() => {
    console.log("lipsum file created")
    return fs.promises.readFile(path.join(__dirname, "lipsum.txt"))
  })
  .then((data) => {
    console.log("lipsum file read success")
    return (data.toString())
  })
  .then((data) => {
    return fs.promises.writeFile(path.join(__dirname, "new_lipsum.txt"), data)
  })
  .then(() => {
    console.log("new lipsum file created")
    return fs.promises.unlink(path.join(__dirname, "lipsum.txt"))
  })
  .then(() => {
    console.log("Original lipsum file delted")
  })
  .catch((err) => {
    console.log(err)
  })




//     B. using callbacks

function lipsum_callbacks(lipsum, callback) {

  fs.writeFile(path.join(__dirname, "lipsum.txt"), lipsum, (err) => {
    if (err) {
      callback(err)
    }
    else {
      callback(null, "Lipsum file created ")
      fs.readFile(path.join(__dirname, "lipsum.txt"), (err, data) => {
        if (err) {
          callback(err)
        }
        else {
          callback(null, "Lipsum file read success")
          fs.writeFile(path.join(__dirname, "new_lipsum.txt"), data, (err) => {
            if (err) {
              callback(err)
            }
            else {
              callback(null, "New file created ")
              fs.unlink(path.join(__dirname, "lipsum.txt"), (err) => {
                if (err) {
                  callback(err)
                }
                else {
                  callback(null, "Original lipsum file deleted")
                }
              })
            }
          })
        }
      })
    }
  })
}
let lipsum = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ornare turpis sit amet ex feugiat tincidunt. Duis vel odio nisl. Nam in venenatis odio. Morbi commodo dui nec neque euismod, sit amet rhoncus elit accumsan. Sed ut venenatis erat. Ut at condimentum metus, vel iaculis leo. Nam mattis efficitur felis nec fermentum. Nullam efficitur augue aliquet, ullamcorper ante quis, semper elit. Quisque rutrum mauris quis risus commodo, lobortis aliquet nunc consectetur. Duis tempus quam sed elit finibus ultricies. Ut mi odio, fermentum nec ipsum et, iaculis fringilla mauris. Quisque ut tortor non justo hendrerit dictum. Nullam ornare porta purus ac pulvinar. Duis auctor bibendum efficitur. Sed iaculis ligula a felis ultrices, vitae fermentum risus aliquam. Proin cursus suscipit nunc, a sagittis ex rutrum quis.

Suspendisse ultricies, lacus eget convallis fermentum, urna arcu lacinia libero, eu sodales ipsum felis ut lectus. Suspendisse egestas tellus a nisi hendrerit pretium. Sed commodo eget felis a dapibus. Mauris ac erat a lacus ultricies porta a in risus. Integer a pulvinar mauris, ac congue eros. Donec commodo urna lorem, ac scelerisque enim condimentum ac. Aliquam quis arcu libero. Fusce et eleifend nulla. Curabitur a ligula dapibus, luctus augue eget, vestibulum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus lobortis, sem a finibus congue, ex turpis cursus odio, vitae vulputate nisl odio sed odio. Mauris rhoncus purus quis magna tristique, sed hendrerit felis vestibulum.

Nam sit amet metus at massa vehicula euismod non nec libero. Aliquam erat volutpat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris feugiat dignissim feugiat. Pellentesque varius facilisis purus, nec posuere enim pulvinar sed. Aliquam eleifend, lectus vel accumsan rutrum, dolor est aliquet dui, in dictum velit augue at nibh. Curabitur porttitor venenatis vestibulum.

Nunc elementum, dui eu feugiat feugiat, nulla tortor consequat libero, a posuere ligula mauris ut lectus. Fusce at sapien lacus. Aenean faucibus neque ac ornare rhoncus. Vestibulum dictum dolor ac nisi sollicitudin viverra eget at nisi. Duis non pellentesque orci, ut euismod lorem. Aliquam mattis urna et dapibus dictum. Fusce ut lacus a orci consequat pulvinar nec eu risus. Quisque eget luctus tellus, eget tristique lectus. Integer ornare dapibus interdum. Maecenas condimentum turpis ut tempor gravida. Aenean tempor iaculis sodales. Sed quis sapien ut lectus placerat congue tincidunt sit amet enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu sodales nibh.

Donec consequat lectus leo, eu vestibulum quam faucibus quis. Proin porta ex tempus arcu pulvinar, vel faucibus lacus facilisis. Nunc eleifend, metus at ultricies consectetur, eros risus vehicula magna, viverra placerat magna elit sit amet risus. Curabitur semper lectus in massa ultricies, tincidunt sodales mi vestibulum. Nulla leo sapien, auctor sed orci vel, finibus maximus ante. Donec faucibus porta venenatis. Ut condimentum tempus nisi. Nunc ornare nisl a lorem pharetra, sit amet faucibus ipsum commodo. Ut neque nisi, interdum non lorem sit amet, laoreet rhoncus justo.`

lipsum_callbacks(lipsum, (err, data) => {
  if (err) {
    console.log(err)
  }
  else {
    console.log(data)
  }
})




// Q3. You may use any data you want as the `user` object 
//     as long as it represents a valid user 
//     (eg: For you, for your friend, etc)

//     All calls must be chained unless mentioned otherwise.


function login(user, val) {
  if (val % 2 === 0) {
    return Promise.resolve(user);
  } else {
    return Promise.reject(new Error("User not found"));
  }
}

function getData() {
  return Promise.resolve([
    {
      id: 1,
      name: "Test",
    },
    {
      id: 2,
      name: "Test 2",
    }
  ]);
}



// Use appropriate methods to 
// A. login with value 3 and call getData once login is successful

login({
  id: 1,
  name: "Deepank"
}, 3)
  .then(() => {
    console.log("Login successfull")
    return getData()
  })
  .then((val) => {
    console.log(val)
  })
  .catch((err) => {
    console.log(err)
  })



// B. Write logic to logData after each activity for each user. 
// Following are the list of activities
//     "Login Success"
//     "Login Failure"
//     "GetData Success"
//     "GetData Failure"

//     Call log data function after each activity to log 
//     that activity in the file.

//     All logged activity must also include the timestamp 
//     for that activity.


function logData(user, activity) {

  let activityFileName = "activities.txt"

  let content = user + " " + activity + " " + new Date(Date.now()) + "\n"
  fs.promises.appendFile(path.join(__dirname, activityFileName), content)
    .then(() => {
      console.log("activity added successfully")
    })
    .catch((err) => {
      console.log(err)
    })

}
logData("Nick", "Login Success");
logData("Roger", "GetData failure");
logData("Nick", "GetData success");
logData("Bruce", "Login failure");
logData("Tony", "Login Success");


